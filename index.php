<?php

class events_plugin {

	private function check($stack, $event, $current, $cmp = 0, $left = true) {
		if (count($stack)) {
			$name = array_shift($stack);
			$item = intval($event[$name]);
			$item_2 = isset($event[$name . '_2']) ? intval($event[$name . '_2']) : $item;
			
			if ($cmp == 0) {
				if ($item > $item_2) { 
					if ($current[$name] < $item && $current[$name] > $item_2) return false;
					elseif ($current[$name] == $item) return $this->check($stack, $event, $current, -1, true);
					elseif ($current[$name] == $item_2) return $this->check($stack, $event, $current, -1, false);
				} elseif ($item < $item_2) {
					if (($current[$name] < $item || $current[$name] > $item_2)) return false;
					elseif ($current[$name] == $item) return $this->check($stack, $event, $current, 1, true);
					elseif ($current[$name] == $item_2) return $this->check($stack, $event, $current, 1, false);
				} elseif ($item == $item_2) {
					if ($current[$name] != $item) return false;
					else return $this->check($stack, $event, $current, 0);
				}
			} elseif ($cmp < 0) {
				if ($left && $current[$name] > $item) return false;
				elseif (!$left && $current[$name] < $item_2) return false;
				elseif ($current[$name] == $item) return $this->check($stack, $event, $current, -1, true);
				elseif ($current[$name] == $item_2) return $this->check($stack, $event, $current, -1, false);
			} elseif ($cmp > 0) {
				if ($left && $current[$name] < $item) return false;
				elseif (!$left && $current[$name] > $item_2) return false;
				elseif ($current[$name] == $item) return $this->check($stack, $event, $current, 1, true);
				elseif ($current[$name] == $item_2) return $this->check($stack, $event, $current, 1, false);
			}
		}
		return true;
	}

	public function common($params) {
		if (strpos($params, 'event_')) {
			$event_set = array();

			include 'config.php';

			$current = array();
			$current['wday'] = intval(date('w'));
			$current['day'] = intval(date('j'));
			$current['month'] = intval(date('n'));
			$current['year'] = intval(date('Y'));
			$current['hour'] = intval(date('G'));
			$current['minute'] = intval(date('i'));

			$items = array('year', 'month', 'day', 'hour', 'minute');
			if ($event_set && is_array($event_set) && count($event_set)) {
				foreach ($event_set as $index => $event) {
					if (isset($event['active']) && $event['active']) {
						$wday_ok = true;
						if (isset($event['wday'])) {
							$wdays = explode(',', $event['wday']);
							if (count($wdays) && !in_array($current['wday'], $wdays))
								$wday_ok = false;
						}
						$dday_ok = true;
						if (isset($event['period']) && $event['period']) {
							$stack = array();
							foreach ($items as $item) {
								if (isset($event[$item])) {
									$stack[] = $item;
								} elseif (count($stack)) {
									if (!$this->check($stack, $event, $current)) {
										$dday_ok = false;
										break;
									}
									$stack = array();
								}
							}
							if (count($stack) && !$this->check($stack, $event, $current)) {
								$dday_ok = false;
							}
						} else {
							foreach ($items as $item) {
								if (isset($event[$item]) && isset($current[$item]) && intval($event[$item]) != $current[$item]) {
									$dday_ok = false;
									break;
								}
							}
						}
						$value = ($wday_ok && $dday_ok && isset($event['text']) ? $event['text'] : '');
						$params = preg_replace('#{{\s*event_' . $index . '\s*}}#i', $value, $params);
					}
				}
			}
		} 
		return $params;
	}
}
